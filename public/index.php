<?php

require __DIR__ . '/../vendor/autoload.php';

/*
 * Init autoloader
 */
$loader = new Nette\Loaders\RobotLoader();

// Load environment variables
$dotEnv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotEnv->load();

// Add directories for RobotLoader to index
$loader->addDirectory(__DIR__ . '/../app');
$loader->addDirectory(__DIR__ . '/../libs');

// And set caching to the 'temp' directory
$loader->setTempDirectory(__DIR__ . '/../temp');
$loader->register(); // Run the RobotLoader

/*
 * Instantiate correct app based on requested URI.
 */
$uri = filter_input(INPUT_SERVER, "PATH_INFO", FILTER_SANITIZE_URL);
$app = preg_match("#^/(admin)/?.*#", $uri) ? "Backend" : "Frontend";

$appClass = 'App\\' . $app . '\\' . $app . 'App';

$app = new $appClass($app, $uri);
$app->run();
