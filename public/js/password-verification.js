window.addEventListener("DOMContentLoaded", () => {

    const password = document.getElementById("password");
    const passwordVerif = document.getElementById("password-verification");
    const passwordMessage = document.getElementById("password-verification-message");

    function checkPass() {
        const check = password.value === passwordVerif.value;
        passwordMessage.textContent = check ? "" : "Les mots de passe ne correspondent pas";
        password.classList.toggle("is-danger", !check);
        passwordVerif.classList.toggle("is-danger", !check);
    }

    password.addEventListener("keyup", checkPass);
    passwordVerif.addEventListener("keyup", checkPass);
});

