# OCR Mon Blog

Blog made for [My First Blog project](https://openclassrooms.com/fr/projects/7/assignment) in PHP/Symfony training on Openclassrooms.

## Installation

First of all, make sure you have php and MariaDB installed on your system. Then, clone this repository :

```bash
git clone https://gitlab.com/MarJaM/ocr-mon-blog.git
```

To make the app fully functional, you'll need to [install Composer](https://getcomposer.org/download/) :

```bash
cd ocr-mon-blog
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

Once Composer ready, use it to install dependencies :
```bash
php composer.phar install
```

Depending on your system, you might have to install php-yaml as well. On Ubuntu, just run :

```bash
sudo apt install php-yaml
```

## Configuration

Before launching the app, you'll need to set some setting in a .env file. To get the template, just run :

```bash
cp .env.template .env
```

Open the `.env` file just created and adjust settings according to your system.

Once everything installed and configured, you'll need to init the database. 
You can use the `init-data.sql` dump file to start with a set of demo data. 
See [mysql documentation](https://dev.mysql.com/doc/refman/5.7/en/reloading-sql-format-dumps.html) for more information.

## Run the app

The app can be tested directly with php built-in web server:

```bash
cd public
php -S localhost:8000
```

You can now test the app on http://localhost:8000

If you used the `init-data.sql` dump file, default username / password are :
 
 * `admin@user.fr` / `admin`
 * `user@user.fr` / `user`

