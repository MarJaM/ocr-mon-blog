-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: 
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `monblog`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `monblog` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `monblog`;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(32) NOT NULL,
  `content` varchar(1024) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_comments_fk` (`post_id`),
  CONSTRAINT `posts_comments_fk` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Marius','Mon commentaire',1,'2020-03-31 19:01:14',6),(6,'Hélène','Un peu court mais très intéressant.',1,'2020-04-03 10:29:57',6),(8,'Jean','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Auctor augue mauris augue neque. Eget nunc lobortis mattis aliquam faucibus purus in massa. Enim nunc faucibus a pellentesque sit amet porttitor.',1,'2020-04-03 11:20:15',6),(9,'Alphonse','Un peu juste !!',1,'2020-04-03 13:13:24',7),(10,'Marius','Pas souvent qu&#39;on voit des articles en latin !',0,'2020-04-10 09:28:33',2),(11,'Jean Gabin','Commentaire de Jean',1,'2020-04-14 18:16:44',7),(12,'Marius','Pas top cet article.',0,'2020-04-17 10:17:24',3),(13,'Blanche-Neige','On n&#39;y parle pas de pommes...',0,'2020-04-17 12:03:48',2);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `chapo` varchar(1024) NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_posts_fk` (`author_id`),
  CONSTRAINT `users_posts_fk` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (2,'In iaculis nuncnan','&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ultricies, ante ut finibus ultricies, ipsum augue fermentum velit, sit amet commodo velit nibh ac lectus. Fusce bibendum porta justo. Praesent lacus turpis, suscipit quis ex quis cras amet.&lt;/p&gt;','&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. &lt;strong&gt;Imperdiet proin fermentum leo vel orci porta non pulvinar neque&lt;/strong&gt;. Hac habitasse platea dictumst quisque sagittis purus sit amet. Proin libero nunc consequat interdum varius. Pretium vulputate sapien nec sagittis aliquam malesuada bibendum arcu. Vitae et leo duis ut diam quam nulla porttitor massa. Nunc scelerisque viverra mauris in aliquam. Commodo quis imperdiet massa tincidunt nunc. Odio ut sem nulla pharetra diam sit amet nisl. Mus mauris vitae ultricies leo integer malesuada. Nulla facilisi nullam vehicula ipsum a. In ornare quam viverra orci sagittis eu. Semper eget duis at tellus at.&lt;/p&gt;\r\n&lt;p&gt;Sit amet mattis vulputate enim nulla aliquet. Et odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Tincidunt augue interdum velit euismod in pellentesque massa. Ac ut consequat semper viverra nam libero justo. Quis blandit turpis cursus in hac habitasse platea dictumst. Id venenatis a condimentum vitae. Pellentesque elit eget gravida cum sociis. Turpis in eu mi bibendum neque egestas congue quisque egestas. Tortor aliquam nulla facilisi cras fermentum. Non pulvinar neque laoreet suspendisse. Mi bibendum neque egestas congue. Volutpat maecenas volutpat blandit aliquam etiam erat. Nisi quis eleifend quam adipiscing vitae. Vitae turpis massa sed elementum tempus. Duis ut diam quam nulla porttitor. Volutpat sed cras ornare arcu dui vivamus arcu felis. Integer vitae justo eget magna fermentum.&lt;/p&gt;','2020-03-18 00:00:00','2020-04-17 12:51:40',1),(3,'Hac habitasse platea','&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.&lt;/p&gt;','&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus urna duis convallis convallis tellus id interdum velit laoreet. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Imperdiet proin fermentum leo vel orci porta non pulvinar neque. Hac habitasse platea dictumst quisque sagittis purus sit amet. Proin libero nunc consequat interdum varius. Pretium vulputate sapien nec sagittis aliquam malesuada bibendum arcu. Vitae et leo duis ut diam quam nulla porttitor massa. Nunc scelerisque viverra mauris in aliquam. Commodo quis imperdiet massa tincidunt nunc. Odio ut sem nulla pharetra diam sit amet nisl. Mus mauris vitae ultricies leo integer malesuada. Nulla facilisi nullam vehicula ipsum a. In ornare quam viverra orci sagittis eu. Semper eget duis at tellus at.&lt;/p&gt;\r\n&lt;p&gt;Sit amet mattis vulputate enim nulla aliquet. Et odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Tincidunt augue interdum velit euismod in pellentesque massa. Ac ut consequat semper viverra nam libero justo. Quis blandit turpis cursus in hac habitasse platea dictumst. Id venenatis a condimentum vitae. Pellentesque elit eget gravida cum sociis. Turpis in eu mi bibendum neque egestas congue quisque egestas. Tortor aliquam nulla facilisi cras fermentum. Non pulvinar neque laoreet suspendisse. Mi bibendum neque egestas congue. Volutpat maecenas volutpat blandit aliquam etiam erat. Nisi quis eleifend quam adipiscing vitae. Vitae turpis massa sed elementum tempus. Duis ut diam quam nulla porttitor. Volutpat sed cras ornare arcu dui vivamus arcu felis. Integer vitae justo eget magna fermentum.&lt;/p&gt;\r\n&lt;p&gt;Id velit ut tortor pretium viverra suspendisse potenti nullam ac. Porttitor eget dolor morbi non arcu risus quis. Ut tellus elementum sagittis vitae. Vel elit scelerisque mauris pellentesque pulvinar pellentesque. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus vitae. At auctor urna nunc id cursus metus aliquam eleifend mi. Vulputate ut pharetra sit amet aliquam. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Dui nunc mattis enim ut tellus elementum sagittis vitae et. Enim sit amet venenatis urna. Arcu cursus euismod quis viverra nibh cras pulvinar. Porta lorem mollis aliquam ut. Blandit volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque. Metus aliquam eleifend mi in nulla. Sed arcu non odio euismod. Suspendisse ultrices gravida dictum fusce ut placerat orci. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Vel fringilla est ullamcorper eget nulla facilisi. Commodo viverra maecenas accumsan lacus vel facilisis. Suspendisse in est ante in nibh mauris cursus mattis molestie.&lt;/p&gt;','2020-03-20 00:00:00','2020-04-14 12:24:34',1),(6,'Un nouvel article','&lt;p&gt;Une courte introduction &agrave; cet article.&lt;/p&gt;','&lt;p&gt;Pas beaucoup de chose &agrave; dire...&lt;/p&gt;','2020-04-01 11:30:04','2020-04-02 15:34:03',1),(7,'Encore un nouvel article','&lt;p&gt;Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla&lt;/p&gt;','&lt;p style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Et toujours pas grand chose &agrave; dire !&lt;/em&gt;&lt;/p&gt;','2020-04-03 11:30:19','2020-04-24 11:57:22',4);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `is_site_owner` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(256) DEFAULT NULL,
  `role` varchar(5) DEFAULT 'user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Marius','Jammes','mj@mariusjammes.fr','$2y$10$FOO2floWYSAIngU9fVkfG.sTjazv1fgk5MRJJWgvNkukQ3AbRVMCu',1,'Développeur PHP en alternance chez microDON, je mêle code, éthique et solidarité.','admin'),(2,'Jean','Gabin','jean@gabin.fr','$2y$10$5P4Xe6Vd3hpfW6teNHAVfeHAbcj3xXMXjjaWgzp2oZfAGuk8cVQoG',0,'Un acteur parmi d&#39;autres.','user'),(3,'Jeanne','Calment','jeanne@calment.fr','$2y$10$99AX6iiH2T1cfDbYFRK8EOZKTbUaBk/1MW2GCx3wsVMOHoaRxIM.i',0,'','user'),(4,'Louis','Chedid','louis@chedid.fr','$2y$10$fjf3SiMwdXoQ2JgpoOI5nubbevBmmAF3Zmf5g8LreLWnYOXNezJFa',0,'Blabla','admin'),(5,'User','User','user@user.fr','$2y$10$l.XoccsaGgX3sOkly4Vi3uXuikvScMyN2mQxF5kpWRieM91USsZkK',0,'','user'),(6,'User','Admin','admin@user.fr','$2y$10$p74Syv35TZcEtZUjkiBqZusrmxav6fLQLlDhGmzqz7vf.rVvKmEF6',0,'Je suis l&#39;administrateur par défaut.','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-24 13:06:05
