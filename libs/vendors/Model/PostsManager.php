<?php


namespace Model;


use BlogApp\Manager;

class PostsManager extends Manager
{

    public function initModuleName()
    {
        $this->moduleName = "Posts";
    }

    public function initEntityName()
    {
        $this->entityName = "Post";
    }

    public function getList(
        string $where = null,
        string $select = "*",
        string $from = null,
        string $orderBY = null
    ) {
        $module = strtolower($this->moduleName);

        if ($select === "*") {
            $select = $module . ".*, users.firstname, users.lastname";
        }
        $from = $from ?? "posts, users";
        $where = $where ?? "users.id = " . $module . ".author_id";
        $orderBY = "creation_date DESC";

        return parent::getList($where, $select, $from, $orderBY);
    }

    public function get($where, string $select = "*", string $from = null)
    {
        $module = strtolower($this->moduleName);

        if (is_numeric($where)) {
            $where = $module . ".id=" . $where . " AND " . $module . ".author_id = users.id";
        }
        if ($select === "*") {
            $select = $module . ".*, users.firstname, users.lastname";
        }

        $from = $from ?? $module . ", users";


        return parent::get($where, $select, $from);
    }

}