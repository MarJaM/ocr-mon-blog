<?php


namespace Model;


use BlogApp\Entity;
use BlogApp\Manager;

class CommentsManager extends Manager
{

    public function initModuleName()
    {
        $this->moduleName = "Comments";
    }

    public function initEntityName()
    {
        $this->entityName = "Comment";
    }

    /**
     * @param int $articleId
     * @param bool|null $published If null, get all articles.
     * @return Entity[]
     */
    public function getArticleRelatedComments(int $articleId, bool $published = null)
    {
        $where = "post_id=" . $articleId;
        if ($published !== null) {
            $where .= " AND is_published=" . (int)$published;
        }
        return $this->getList($where);
    }

    public function getList(
        string $where = null,
        string $select = "*",
        string $from = null,
        string $orderBY = null
    ) {
        $orderBY = "creation_date ASC";
        return parent::getList($where, $select, $from, $orderBY);
    }

}