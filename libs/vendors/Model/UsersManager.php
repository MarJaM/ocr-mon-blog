<?php


namespace Model;


use BlogApp\Manager;

class UsersManager extends Manager
{

    public function initModuleName()
    {
        $this->moduleName = "Users";
    }

    public function initEntityName()
    {
        $this->entityName = "User";
    }

}