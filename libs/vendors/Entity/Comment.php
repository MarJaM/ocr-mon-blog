<?php


namespace Entity;


use BlogApp\Entity;
use DateTime;

/**
 * Class Comment
 * @package Entity
 */
class Comment extends Entity
{

    /**
     * @var string $author
     * @filter string
     * @constraint NOT_NULL
     */
    protected $author;

    /**
     * @var string $content
     * @filter string
     * @constraint NOT_NULL
     */
    protected $content;

    /**
     * @var bool $isPublished
     */
    protected $isPublished;

    /**
     * @var DateTime $creationDate
     */
    protected $creationDate;

    /**
     * @var int $post_id
     */
    protected $post_id;

}