<?php


namespace Entity;


use BlogApp\Entity;

/**
 * Class User
 * @package Entity
 */
class User extends Entity
{

    /**
     * @var string $firstname
     * @filter string
     * @constraint NOT_NULL
     */
    protected $firstname;

    /**
     * @var string $lastname
     * @filter string
     * @constraint NOT_NULL
     */
    protected $lastname;

    /**
     * @var bool $isSiteOwner
     */
    protected $isSiteOwner;

    /**
     * @var string $email
     * @filter email
     * @constraint NOT_NULL
     */
    protected $email;

    /**
     * @var string
     * @filter string
     * @constraint NOT_NULL
     */
    protected $password;

    /**
     * @var string
     * @filter string
     */
    protected $description;

    /**
     * @var string
     */
    protected $role;

}