<?php


namespace Entity;


use BlogApp\Entity;
use DateTime;

/**
 * Class Post
 * @package Entity
 */
class Post extends Entity
{

    /**
     * @var string $title
     * @filter string
     * @constraint NOT_NULL
     */
    protected $title;

    /**
     * @var string $chapo
     * @filter full_special_chars
     * @constraint NOT_NULL
     */
    protected $chapo;

    /**
     * @var string $content
     * @filter full_special_chars
     * @constraint NOT_NULL
     */
    protected $content;

    /**
     * @var DateTime $creationDate
     */
    protected $creationDate;

    /**
     * @var DateTime $lastUpdate
     */
    protected $lastUpdate;

    /**
     * @var int $author_id
     * @filter int
     */
    protected $author_id;

}