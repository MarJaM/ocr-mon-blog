<?php


namespace BlogApp;

/**
 * Class Route
 * Represent a route set in routes.yaml
 * @package BlogApp
 */
class Route
{
    /**
     * @var string $controller
     */
    protected $controller;

    /**
     * @var string $pathPrefix
     */
    protected $pathPrefix;

    /**
     * @var string $path
     */
    protected $path;

    /**
     * @var string $action
     */
    protected $action;


    public function __construct($path, $controller, $pathPrefix = "")
    {
        $this->pathPrefix = $pathPrefix;
        $this->path = $path === "/" ? "/?" : $path;
        $this->controller = $controller;
        $this->splitControllerAndAction($controller);
    }

    /**
     * @param string $controller Controller string from routes.yaml
     */
    protected function splitControllerAndAction($controller)
    {
        $strings = explode("::", $controller);
        $this->controller = $strings[0];
        $this->action = $strings[1];
    }

    /**
     * Check whether this route match the current uri
     * @param string $uri
     * @return false|int
     */
    public function match(string $uri)
    {
        $pattern = '#^' . $this->pathPrefix . $this->path . '/?$#';
        $pattern = preg_replace("#{[a-z]+_id}#", "\d+", $pattern);
        return preg_match($pattern, $uri);
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

}