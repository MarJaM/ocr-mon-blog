<?php


namespace BlogApp;

/**
 * Class Router
 * @package BlogApp
 */
class Router extends Component
{
    /**
     * @var array $routes
     */
    protected $routes = [];

    /**
     * @return Route
     */
    public function getRoute()
    {
        $uri = $this->app->getUri();
        foreach ($this->routes as $route) {
            if ($route->match($uri)) {
                return $route;
            }
        }
        return null;
    }

    /**
     * @param Route $route
     */
    public function addRoute($route)
    {
        array_push($this->routes, $route);
    }

}