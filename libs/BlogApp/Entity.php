<?php


namespace BlogApp;


use ArrayAccess;
use DateTime;
use Exception;
use ReflectionClass;

/**
 * Class Entity
 * @package BlogApp
 */
abstract class Entity implements ArrayAccess
{
    /**
     * @var int $id
     */
    protected $id;

    /**
     * @var ReflectionClass
     */
    protected $reflector;

    /**
     * Entity constructor.
     * @throws \ReflectionException|Exception
     */
    public function __construct()
    {
        $this->reflector = new ReflectionClass($this);
        $this->formatDatesProperties();
    }

    /**
     * Format DateTime properties at __construct time.
     * @throws Exception
     */
    protected function formatDatesProperties()
    {
        $properties = $this->reflector->getProperties();
        foreach ($properties as $property) {
            $docComment = $property->getDocComment();
            if (preg_match('#\* @var DateTime#', $docComment)) {
                $propertyName = $property->getName();
                if (!empty($this->$propertyName)) {
                    $this->$propertyName = new DateTime($this->$propertyName);
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $var
     * @return mixed|null
     */
    public function offsetGet($var)
    {
        return $this->$var ?? null;
    }

    /**
     * @param mixed $var
     * @param mixed $value
     * @throws Exception
     */
    public function offsetSet($var, $value)
    {
        $method = 'set' . ucfirst($var);

        if (property_exists($this, $var) && is_callable([$this, $method])) {
            $this->$method($value);
        } elseif (property_exists($this, $var)) {
            $this->$var = $value;
        } else {
            throw new Exception("Impossible de set " . $var . " car inexistante.");
        }
    }

    /**
     * @param mixed $var
     * @return bool
     */
    public function offsetExists($var)
    {
        return isset($this->$var);
    }

    /**
     * @param mixed $var
     * @throws Exception
     */
    public function offsetUnset($var)
    {
        throw new Exception('Impossible de supprimer une quelconque valeur');
    }

    /**
     * @param string $tagName
     * @return array
     */
    public function getPropertiesInfo(string $tagName)
    {
        $properties = $this->reflector->getProperties();
        $infos = [];

        foreach ($properties as $property) {
            $filter = preg_match("/\* @" . $tagName . " (\w*)/", $property->getDocComment(), $matches);
            if ($filter) {
                $infos[$property->getName()] = $matches[1];
            }
        }

        return $infos;
    }
}