<?php


namespace BlogApp;

session_start();

class User extends Component
{
    /**
     * @var string
     */
    protected $flash = "";

    /**
     * @var bool
     */
    protected $authenticated;

    /**
     * @var Entity|string[]
     */
    protected $user = ["role" => "visitor"];

    /**
     * User constructor.
     * @param $app
     * @throws \Exception
     */
    public function __construct($app)
    {
        parent::__construct($app);
        if ($this->getSession("flash")) {
            $this->setFlash($this->getSession("flash"));
            $this->setSession("flash", "");
        }

        if ($this->getSession('userId')) {
            $usersManager = Managers::getManagerOf("users");
            $this->setUser($usersManager->get("id=" . $this->getSession('userId')));
        }
    }

    /**
     * @return Entity|string[]
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Entity $user
     */
    public function setUser(Entity $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getFlash(): string
    {
        return $this->flash;
    }

    /**
     * @param string $flash
     */
    public function setFlash(string $flash)
    {
        $this->flash = $flash;
    }

    /**
     * @param string $key
     * @param $value
     */
    public function setSession(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function getSession(string $key)
    {
        return $_SESSION[$key] ?? null;
    }

    /**
     * @return array
     */
    public function resetSession()
    {
        return $_SESSION = [];
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return isset($_SESSION['auth']) && $_SESSION["auth"];
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->user['role'] === "admin";
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->user[$name];
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->user && isset($this->user[$name]);
    }
}