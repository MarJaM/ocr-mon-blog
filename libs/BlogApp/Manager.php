<?php


namespace BlogApp;


use PDO;

abstract class Manager
{
    /**
     * @var PDO $dao
     */
    protected $dao;

    /**
     * @var string $moduleName
     */
    protected $moduleName;

    /**
     * @var string $entityName
     */
    protected $entityName;

    /**
     * @var string
     */
    protected $listSqlQuery;

    /**
     * @var string
     */
    protected $getSqlQuery;


    abstract public function initModuleName();

    abstract public function initEntityName();

    public function __construct(PDO $dao)
    {
        $this->dao = $dao;
        $this->initModuleName();
        $this->initEntityName();
    }

    /**
     * @param string $sql
     * @return Entity[]
     */
    protected function queryAndFetch(string $sql)
    {
        $query = $this->dao->query($sql, PDO::FETCH_CLASS, $this->getEntityName());
        return $query->fetchAll();
    }

    /**
     * Return the list of records of the manager's module.
     * @param string|null $where
     * @param string $select
     * @param string|null $from
     * @param string|null $orderBY
     * @return Entity[]
     */
    public function getList(
        string $where = null,
        string $select = "*",
        string $from = null,
        string $orderBY = null
    ) {
        $from = $from ?? strtolower($this->moduleName);

        $sql = "SELECT " . $select . " FROM " . $from;
        if (is_string($where)) {
            $sql .= " WHERE " . $where;
        }

        if (is_string($orderBY)) {
            $sql .= " ORDER BY " . $orderBY;
        }

        return $this->queryAndFetch($sql);
    }

    /**
     * Get a record from the database given its ID.
     * @param int|string $where Can be set to the ID or a more complex SQL where condition.
     * @param string $select
     * @param string|null $from
     * @return Entity
     */
    public function get($where, string $select = "*", string $from = null)
    {
        if (is_numeric($where)) {
            $where = "id=" . $where;
        }
        $from = $from ?? strtolower($this->moduleName);

        $sql = "SELECT " . $select . " FROM " . $from . " WHERE " . $where;

        $result = $this->queryAndFetch($sql);
        return $result[0];
    }

    /**
     * Delete a record in database given its ID.
     * @param int $itemId
     */
    public function delete(int $itemId)
    {
        $sql = "DELETE FROM " . strtolower($this->moduleName) . " WHERE id=" . $itemId;
        $this->dao->prepare($sql)->execute();
    }

    /**
     * Create a new return in the manager's module given an associative array of params.
     * @param array $params
     * @return string
     */
    public function create(array $params)
    {
        $columns = array_keys($params);

        $columnsStr = implode(",", $columns);
        $valuesStr = array_map(
            function ($item) {
                return ":" . $item;
            },
            $columns
        );
        $valuesStr = implode(",", $valuesStr);

        $sql = "INSERT INTO " . strtolower($this->moduleName) . " (" . $columnsStr . ") VALUES (" . $valuesStr . ")";
        $query = $this->dao->prepare($sql)->execute($params);
        return $query ? $this->dao->lastInsertId() : false;
    }

    /**
     * Update a record given its id and an associative array of params.
     * @param int $itemId
     * @param array $params
     */
    public function update(int $itemId, array $params)
    {
        $columns = array_keys($params);

        $entriesStr = array_map(
            function ($column) {
                return $column . "=:" . $column;
            },
            $columns
        );
        $entriesStr = implode(",", $entriesStr);

        $sql = "UPDATE " . strtolower($this->moduleName) . " SET " . $entriesStr . " WHERE id=" . $itemId;
        $this->dao->prepare($sql)->execute($params);
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return "Entity\\" . $this->entityName;
    }
}