<?php

namespace BlogApp;


use Exception;
use ReflectionClass;

/**
 * Class BlogApp
 * @package BlogApp
 */
abstract class BlogApp
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var Router $router
     */
    protected $router;

    /**
     * @var Route[] $routes
     */
    protected $routes = [];

    /**
     * @var string
     */
    protected $path;

    /**
     * @var User
     */
    public $user;

    /**
     * BlogApp constructor.
     * @param $name
     * @param $uri
     * @throws \ReflectionException
     * @throws Exception
     */
    public function __construct($name, $uri)
    {
        $this->uri = empty($uri) ? "/" : $uri;
        $this->user = new User($this);
        $this->name = $name;
        $this->router = new Router($this);
        $this->initRoutes();
        $this->initPath();
    }

    /**
     * Get all routes from the yaml file.
     */
    protected function initRoutes()
    {
        $routesFile = yaml_parse_file(__DIR__ . '/../../app/' . $this->name . '/Config/routes.yaml');
        $routes_prefix = $routesFile['routes_prefix'] ?? "";
        $routes = $routesFile['routes'];
        foreach ($routes as $route) {
            array_push($this->routes, new Route($route['path'], $route['controller'], $routes_prefix));
        }
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @throws \ReflectionException
     */
    protected function initPath()
    {
        $reflector = new ReflectionClass($this);
        $this->path = dirname($reflector->getFileName());
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Run the app.
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function run()
    {
        try {
            foreach ($this->routes as $route) {
                $this->router->addRoute($route);
            }
            $matchedRoute = $this->router->getRoute();

            if ($matchedRoute === null) {
                $this->displayErrorPage("Oups... Cette page n'existe pas.");
            }

            $this->getAndExecController($matchedRoute);
        } catch (Exception $err) {
            error_log($err->getMessage());
            $this->displayErrorPage("Oups... Une erreur inattendue s'est produite. Pas cool :(");
        }
    }

    /**
     * @param Route $route
     */
    protected function getAndExecController(Route $route)
    {
        $controller = $route->getController();
        $controller = new $controller($this);
        $action = $route->getAction();

        $controller->exec($action);
    }

    /**
     * Display an error page.
     * @param string $message Message to be displayed on the user's screen.
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function displayErrorPage($message)
    {
        $page = new Page($this, null, "error", ["message" => $message]);
        $page->display();
        exit;
    }

    /**
     * @param string $uri
     */
    public function redirectTo(string $uri)
    {
        header("Location: " . $uri);
        exit;
    }
}
