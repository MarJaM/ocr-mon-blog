<?php


namespace BlogApp;


abstract class Component
{
    /**
     * @var BlogApp
     */
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }
}