<?php


namespace BlogApp;


use Exception;
use PDO;

/**
 * Class Managers
 * @package BlogApp
 */
class Managers
{
    /**
     * @var array $managers
     */
    protected static $managers = [];

    /**
     * @var PDO
     */
    protected static $dao;

    /**
     * @param string $module
     * @return Manager
     * @throws Exception
     */
    public static function getManagerOf($module)
    {
        if (empty(self::$dao)) {
            self::$dao = DbFactory::getDbConnection();
        }

        if (!isset(self::$managers[$module])) {
            $manager = 'Model\\' . ucfirst($module) . 'Manager';

            if (!class_exists($manager)) {
                throw new Exception("Trying to load an unexistent manager : " . $manager);
            }

            self::$managers[$module] = new $manager(self::$dao);
        }

        return self::$managers[$module];
    }
}