<?php


namespace BlogApp;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Page extends Component
{

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $pageName;

    /**
     * @var array
     */
    protected $data;

    /**
     * Page constructor.
     * @param BlogApp $app
     * @param string|null $moduleName
     * @param string $action
     * @param array $data
     * @throws \Twig\Error\LoaderError
     */
    public function __construct($app, $moduleName, $action, $data = [])
    {
        parent::__construct($app);
        $this->pageName = strtolower($action) . ".html.twig";
        $this->data = $data ?? [];

        $path = $this->app->getPath();
        $twigLoader = new FilesystemLoader($path . '/Templates/');
        if ($moduleName !== null) {
            $twigLoader->addPath($path . '/Modules/' . $moduleName . "/Views/");
        }
        $this->twig = new Environment(
            $twigLoader, [
                           'debug' => true,
                           'autoescape' => 'html'
                       ]
        );
    }

    /**
     * Display a page.
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Exception
     */
    public function display()
    {
        $this->data["user"] = $this->app->user;
        $this->data["uri"] = urlencode($this->app->getUri());
        echo $this->twig->render($this->pageName, $this->data);
    }


}