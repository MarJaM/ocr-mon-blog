<?php


namespace BlogApp;


use Exception;
use PDO;
use PDOException;

/**
 * Class DbFactory
 * @package BlogApp
 */
class DbFactory
{

    /**
     * @return PDO
     * @throws Exception
     */
    public static function getDbConnection()
    {
        try {
            $database = new PDO(
                'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
                getenv('DB_USER'),
                getenv('DB_PASSWORD') ?? null
            );
        } catch (PDOException $e) {
            throw new Exception("Erreur à la connexion à la base de données : " . $e->getMessage());
        }
        $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $database;
    }
}