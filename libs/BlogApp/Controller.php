<?php


namespace BlogApp;

use Exception;
use ReflectionClass;

abstract class Controller extends Component
{
    /**
     * @var string
     */
    protected $moduleName;

    /**
     * @var string|null
     */
    protected $entityName;

    /**
     * @var Manager|null
     */
    protected $moduleManager;

    /**
     * Should return the name of the entity linked to the Controller or null if none.
     * @return string|null
     */
    abstract protected function initEntityName();

    /**
     * Controller constructor.
     * @param BlogApp $app
     * @throws \ReflectionException|Exception
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->moduleName = $this->initModuleName();
        $this->entityName = $this->initEntityName();

        if ($this->entityName !== null) {
            $this->moduleManager = Managers::getManagerOf($this->moduleName);
        }
    }

    /**
     * Get the module name based on the namespace.
     * @return string
     * @throws \ReflectionException
     */
    protected function initModuleName()
    {
        $reflector = new ReflectionClass($this);
        $namespace = explode("\\", $reflector->getNamespaceName());
        return end($namespace);
    }

    /**
     * Execute the action as defined in the route.
     * @param string $action
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function exec(string $action)
    {
        $data = $this->$action() ?? [];
        $this->displayPage($action, $data);
    }

    /**
     * @param string $action
     * @param array $data = null
     * @return string|void
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function displayPage(string $action, array $data = [])
    {
        $action = $action === "update" ? "show" : $action;
        $page = new Page($this->app, $this->moduleName, $action, $data);
        $page->display();
    }

    /**
     * @return Entity[]
     */
    protected function list()
    {
        $data = $this->moduleManager->getList();
        return array_map(
            function ($item) {
                return $this->decodeHtmlParams($item);
            },
            $data
        );
    }

    /**
     * @param int|null $itemId
     * @return Entity
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function show(int $itemId = null)
    {
        $data = $this->moduleManager->get($itemId ?? $this->getIdFromUri());
        if ($data === null) {
            $this->app->displayErrorPage("Oups... cette page n'existe pas.");
        }
        return $this->decodeHtmlParams($data);
    }

    /**
     * Update is some values are posted and display show view.
     * @throws Exception
     */
    protected function update()
    {
        $data = $this->getFilteredPostParams();
        $itemId = $this->getIdFromUri();
        if ($data) {
            $this->checkPostNulls($data);
            $data = $this->filterDataBeforeUpdate($data);
            $this->moduleManager->update($itemId, $data);
        }
        return $this->show($itemId);
    }

    /**
     * Delete the item and redirect to home.
     */
    protected function delete()
    {
        $this->moduleManager->delete($this->getIdFromUri());
        $this->app->redirectTo("/admin");
    }

    /**
     * @throws Exception
     * Insert the new item in the database and redirect to home.
     */
    protected function create()
    {
        $data = $this->getFilteredPostParams();
        if ($data) {
            $this->checkPostNulls($data);
            $data = $this->filterDataBeforeCreate($data);
            return $this->moduleManager->create($data);
        }
    }

    /**
     * Retrieve the item id from the uri
     * @param string $module
     * @return int
     */
    protected function getIdFromUri(string $module = null)
    {
        $module = $module ?? $this->moduleName;
        $uri = explode("/", $this->app->getUri());
        $key = array_search(strtolower($module), $uri);
        filter_var($key, FILTER_VALIDATE_INT);
        return $uri[$key + 1];
    }

    /**
     * @param string|null $entityName
     * @return array
     */
    protected function getFilterArgs(string $entityName = null)
    {
        $entityName = $entityName ?? $this->entityName;
        $filters = $this->getEntity($entityName)->getPropertiesInfo("filter");
        foreach ($filters as $key => $filterName) {
            $filters[$key] = filter_id($filterName);
        }
        return $filters;
    }

    /**
     * @param string $entityName
     * @return Entity
     */
    protected function getEntity(string $entityName)
    {
        $entityClass = "Entity\\" . $entityName;
        return new $entityClass();
    }

    /**
     * Return an array of sanitized values.
     * @return array
     * @throws Exception
     */
    protected function getFilteredPostParams()
    {
        $data = filter_input_array(INPUT_POST, $this->getFilterArgs());
        if ($data && in_array(false, $data, true)) {
            throw new Exception("Data sent in form doesn't pass validation test.");
        }
        return $data ?? [];
    }

    /**
     * Decode entity's param to display them correctly on frontend.
     * @param $data Entity
     * @return mixed
     */
    protected function decodeHtmlParams(Entity $data)
    {
        $paramsToDecode = array_filter(
            $this->getFilterArgs(),
            function ($filterId) {
                return $filterId === FILTER_SANITIZE_FULL_SPECIAL_CHARS;
            }
        );
        foreach ($paramsToDecode as $paramName => $filterId) {
            $data[$paramName] = htmlspecialchars_decode($data[$paramName]);
        }
        return $data;
    }

    /**
     * Throw exception when submitted data is empty when it shouldn't.
     * @param array $data
     * @throws Exception
     */
    protected function checkPostNulls(array $data)
    {
        $constraintsList = $this->getEntity($this->entityName)->getPropertiesInfo("constraint");
        foreach ($constraintsList as $param => $constraint) {
            if ($constraint === "NOT_NULL" && empty($data[$param])) {
                throw new Exception($param . " cannot be empty");
            }
        }
    }

    /**
     * To use in child classes to add custom data after filter validation from POST and before creating the new resource.
     * @param array $data
     * @return array
     */
    protected function filterDataBeforeCreate(array $data)
    {
        return $data;
    }

    /**
     * To use in child classes to add custom data after filter validation from POST and before updating the resource.
     * @param array $data
     * @return array
     */
    protected function filterDataBeforeUpdate(array $data)
    {
        return $data;
    }
}