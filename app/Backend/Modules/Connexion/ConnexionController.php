<?php


namespace App\Backend\Modules\Connexion;


use BlogApp\Controller;
use BlogApp\Managers;
use Exception;

class ConnexionController extends Controller
{

    /**
     * @return string|null
     */
    protected function initEntityName()
    {
        return null;
    }

    /**
     * @param int|null $itemId
     * @return array|\BlogApp\Entity
     * @throws Exception
     */
    protected function show(int $itemId = null)
    {
        $this->signIn();

        $redirectTo = filter_input(
            INPUT_GET,
            "redirectTo",
            FILTER_SANITIZE_STRING
        );

        $redirectTo = $redirectTo ? urldecode($redirectTo) : $this->app->getUri();

        if ($this->app->user->isAuthenticated()) {
            $this->app->redirectTo($redirectTo);
        }

        return [
            "page" => "login",
            "redirectTo" => $redirectTo
        ];
    }

    /**
     * Perform signing in action.
     * @throws Exception
     */
    protected function signIn()
    {
        $credentials = filter_input_array(INPUT_POST, $this->getFilterArgs("User"));
        if ($credentials && $credentials['password'] && $credentials['email']) {
            $user = $this->checkCredentials($credentials);
            if ($user) {
                $this->app->user->setSession('userId', $user['id']);
                $this->app->user->setSession('auth', true);
            }
        }
    }

    /**
     * Logout user and destroy his session
     */
    protected function logout()
    {
        $this->app->user->resetSession();
        $this->app->redirectTo("/");
    }

    /**
     * Check whether sent credentials are valid.
     * @param array $credentials
     * @return \BlogApp\Entity|false
     * @throws Exception
     */
    protected function checkCredentials(array $credentials)
    {
        $usersManager = Managers::getManagerOf("Users");

        try {
            $user = $usersManager->get("email='" . strtolower($credentials['email'] . "'"));
        } catch (Exception $exception) {
            $this->app->user->setFlash("Mauvais identifiant ou mot de passe.");
            return false;
        }

        if (!password_verify($credentials['password'], $user['password'])) {
            $this->app->user->setFlash("Mauvais identifiant ou mot de passe.");
            return false;
        }

        return $user;
    }

}