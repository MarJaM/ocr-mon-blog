<?php


namespace App\Backend\Modules\Posts;


use BlogApp\Controller;
use BlogApp\Entity;
use BlogApp\Managers;
use Exception;
use Model\CommentsManager;
use Model\UsersManager;

class PostsController extends Controller
{
    /**
     * @var CommentsManager
     */
    protected $commentsManager;

    /**
     * @var UsersManager
     */
    protected $usersManager;

    public function __construct($app)
    {
        parent::__construct($app);
        $this->commentsManager = Managers::getManagerOf("comments");
        $this->usersManager = Managers::getManagerOf("users");
    }

    protected function initEntityName()
    {
        return "Post";
    }

    protected function list()
    {
        $articles = parent::list();
        return ["articles" => $articles];
    }

    /**
     * @param int|null $postId
     * @return array|Entity
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws Exception
     */
    protected function show(int $postId = null)
    {
        $article = parent::show();
        $comments = $this->getRelatedComments(false);
        $users = $this->getAdminUsers();
        return ["article" => $article, "comments" => $comments, "users" => $users];
    }

    protected function create()
    {
        $postId = parent::create();
        if ($postId) {
            $this->app->redirectTo("/admin/posts/" . $postId . "/update/");
        }
        return ["users" => $this->getAdminUsers()];
    }

    /**
     * @throws Exception
     */
    protected function delete()
    {
        $this->deleteRelatedComments();
        parent::delete();
    }

    protected function filterDataBeforeUpdate(array $data)
    {
        $data["last_update"] = date("Y-m-d H:i:s");
        return parent::filterDataBeforeUpdate($data);
    }

    /**
     * @param bool|null $published
     * @return Entity[]
     * @throws Exception
     */
    protected function getRelatedComments(bool $published = null)
    {
        return $this->commentsManager->getArticleRelatedComments($this->getIdFromUri(), $published);
    }

    /**
     * @throws Exception
     */
    protected function deleteRelatedComments()
    {
        $comments = $this->getRelatedComments();
        array_walk(
            $comments,
            function ($comment) {
                $this->commentsManager->delete($comment['id']);
            }
        );
    }

    /**
     * @return Entity[]
     */
    protected function getAdminUsers()
    {
        return $this->usersManager->getList("role='admin'");
    }
}