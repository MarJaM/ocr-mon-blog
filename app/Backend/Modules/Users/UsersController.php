<?php


namespace App\Backend\Modules\Users;


use BlogApp\Controller;

class UsersController extends Controller
{

    protected function initEntityName()
    {
        return "User";
    }

    protected function create()
    {
        $userId = parent::create();
        if ($userId) {
            $this->app->user->setSession('flash', "Votre compte utilisateur a bien été créé.");
            $this->app->redirectTo("/admin/login");
        }
    }

    protected function filterDataBeforeCreate(array $data)
    {
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        return parent::filterDataBeforeCreate($data);
    }
}