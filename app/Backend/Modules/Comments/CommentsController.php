<?php


namespace App\Backend\Modules\Comments;


use BlogApp\Controller;

class CommentsController extends Controller
{

    protected function initEntityName()
    {
        return "Comment";
    }

    protected function update()
    {
        $articleId = $this->getRelatedArticleId();
        $this->moduleManager->update($this->getIdFromUri(), ["is_published" => true]);
        $this->redirectToArticle($articleId);
    }

    protected function delete()
    {
        $articleId = $this->getRelatedArticleId();
        $this->moduleManager->delete($this->getIdFromUri());
        $this->redirectToArticle($articleId);
    }

    protected function redirectToArticle($articleId)
    {
        $this->app->redirectTo("/admin/posts/" . $articleId . "/update/");
    }

    protected function getRelatedArticleId()
    {
        $comment = $this->moduleManager->get($this->getIdFromUri());
        return $comment["post_id"];
    }
}