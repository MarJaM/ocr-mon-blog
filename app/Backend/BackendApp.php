<?php


namespace App\Backend;


use App\Backend\Modules\Connexion\ConnexionController;
use BlogApp\BlogApp;
use BlogApp\Route;

class BackendApp extends BlogApp
{

    /**
     * @param Route $route
     * @throws \ReflectionException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function getAndExecController(Route $route)
    {
        if (!$this->user->isAuthenticated() && $route->getPath() !== "/users/registration") {
            return $this->displayLoginPage();
        } elseif ($this->user->isAuthenticated() && !$this->user->isAdmin() && $route->getPath() !== "/logout") {
            $this->redirectTo("/");
        }

        parent::getAndExecController($route);
    }

    /**
     * Display the login page.
     * @throws \ReflectionException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function displayLoginPage()
    {
        $controller = new ConnexionController($this);
        $controller->exec("show");
    }

}