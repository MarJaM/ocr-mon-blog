<?php


namespace App\Frontend\Modules\Comments;


use BlogApp\Controller;

class CommentsController extends Controller
{

    protected function initEntityName()
    {
        return "Comment";
    }

    protected function filterDataBeforeCreate(array $data)
    {
        $data["post_id"] = $this->getIdFromUri("posts");
        return parent::filterDataBeforeCreate($data);
    }

    protected function create()
    {
        parent::create();
        $this->app->redirectTo("/posts/" . $this->getIdFromUri("posts"));
    }

}