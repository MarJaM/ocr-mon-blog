<?php


namespace App\Frontend\Modules\Posts;


use BlogApp\Controller;
use BlogApp\Managers;

class PostsController extends Controller
{

    protected function initEntityName()
    {
        return "Post";
    }

    protected function list()
    {
        $articles = parent::list();
        return ["page" => "blog", "articles" => $articles];
    }

    /**
     * @param int|null $postId
     * @return array|\BlogApp\Entity
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function show(int $postId = null)
    {
        $article = parent::show();
        $commentsManager = Managers::getManagerOf("comments");
        $comments = $commentsManager->getArticleRelatedComments($this->getIdFromUri(), true);
        return ["article" => $article, "comments" => $comments];
    }
}