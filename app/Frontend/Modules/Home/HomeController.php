<?php


namespace App\Frontend\Modules\Home;


use BlogApp\Controller;
use BlogApp\Managers;
use Entity\User;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;

class HomeController extends Controller
{
    /**
     * @var User
     */
    private $_siteOwner;

    public function __construct($app)
    {
        parent::__construct($app);
        $usersManager = Managers::getManagerOf("Users");
        $this->_siteOwner = $usersManager->get("is_site_owner");
    }

    /**
     * @return string|null
     */
    protected function initEntityName()
    {
        return null;
    }

    /**
     * Check whether the contact form has been submitted before displaying page.
     * @param int|null $itemId
     * @return User[]
     * @throws Exception
     */
    protected function show(int $itemId = null)
    {
        $this->_sendMail();
        return ["owner" => $this->_siteOwner];
    }

    /**
     * Send an email after submitting the form on Home page.
     * @throws Exception
     */
    private function _sendMail()
    {
        $data = filter_input_array(
            INPUT_POST,
            [
                "email" => FILTER_VALIDATE_EMAIL,
                "name" => FILTER_SANITIZE_STRING,
                "message" => FILTER_SANITIZE_STRING
            ]
        );

        if (!$data || in_array(false, $data, false)) {
            return;
        }

        $data['message'] .= "\r\n";
        $data['message'] .= "-------------------------------\r\n";
        $data['message'] .= $data['name'];

        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->Host = getenv("SMTP_HOST");
            $mail->SMTPAuth = getenv("SMTP_USERNAME") && getenv("SMTP_PASSWORD");
            $mail->Username = getenv("SMTP_USERNAME");
            $mail->Password = getenv("SMTP_PASSWORD");
            $mail->SMTPSecure = getenv("SMTP_ENCRYPTION");
            $mail->Port = (int)getenv("SMTP_PORT");

            $mail->setFrom("monblog@ocr.com");
            $mail->addReplyTo($data['email']);
            $mail->addAddress(getenv('SEND_EMAILS_TO'));

            $mail->Subject = "Un nouvel email depuis ton blog";
            $mail->Body = $data['message'];

            $mail->send();
            $this->app->user->setFlash("Le message a bien été envoyé.");
        } catch (Exception $e) {
            throw new Exception("Oups... something went wrong while sending email... " . $e->getMessage());
        }
    }
}